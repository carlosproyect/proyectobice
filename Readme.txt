-------------------------------------------
Aplicacion Proyecto Indicadores Comerciales
--------------------------------------------
Autor: Carlos Mujica

-----------------
Especificaciones:
------------------

Leguaje: Angular 8.
Servicdor: NodeJS.

* Antes de comenzar se debe tener instalado NodeJS. https://nodejs.org/es/

-------------------
Ejecutar Aplicación
-------------------

1.- Clonar fuentes con git: git clone https://cmujica0209@bitbucket.org/carlosproyect/proyectobice.git 
	o descargar fuentes de: https://drive.google.com/open?id=1IzFrAuxk250AsYz09oMa2YNxwm5hlbvu
2.- Ingresar a la carpeta: proyectobice/proyectoBICE
3.- Ejecutar comando npm install.
4.- Una vez finalizado el paso 3 ejecutar el comando npm start.
5.- IMPORTANTE! - Antes de ejecutar la aplicación se debe realizar lo indicado en el documento: Instalar CORS Allow CORS.pdf, ya que sin esto no es 
	posible realizar la comunicación con la API por problemas de CORS.
6.- ingresar a la url: http://localhost:4200/home




------------------------
Ejecutar Prueba Unitaria
------------------------

1.- Clonar fuentes con git: git clone https://cmujica0209@bitbucket.org/carlosproyect/proyectobice.git 
2.- Ingresar a la carpeta: proyectobice/proyectoBICE
3.- ejecutar comando ng test
4.- automaticamente se abre chrome y realiza la prueba unitaria.

Se ejecutaran 3 pruebas:

- Creación del componente home.
- Obtencion de los datos para mostrar.
- Mostrar mensaje de error si falla la  API.
