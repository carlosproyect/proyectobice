export class IndicadorData{
    dolar:Dolar;
    cobre:Cobre;
    euro:Euro;
    ipc:Ipc;
    ipv:Ipv;
    oro:Oro;
    plata:Plata;
    uf:Uf;
    utm:Utm;
    yen:Yen;

    constructor(){

    }
}

export class Cobre{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;

    constructor(){
        
    }
}

export class Dolar{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;

    constructor(){
        
    }
}

export class Euro{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string

    constructor(){
        
    }
}

export class Ipc{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;

    constructor(){
        
    }
}

export class Ipv{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;
    
    constructor(){
        
    }
}

export class Oro{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;

    constructor(){
        
    }
}

export class Plata{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;

    constructor(){
        
    }
}

export class Uf{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;

    constructor(){
        
    }
}

export class Utm{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;

    constructor(){
        
    }
}

export class Yen{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;

    constructor(){
        
    }
}

