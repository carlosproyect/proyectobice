import { Component, OnInit } from '@angular/core';
import {PeticionService} from '../services/peticion.service'
import { Indicador } from '../services/model/last-response';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [PeticionService]
})

/** 
 * HomeComponent
 * 
 * componente encargado de levantar el home de la aplicacion.
 * Se obtienen los ultimos valores de los distintos indicadores y se muestran.
 */
export class HomeComponent implements OnInit {

  constructor(private _peticioService : PeticionService) { }

  public indicadores:Indicador;
  public response:any;
  public error:string;

  ngOnInit() {
    this.error="";
    this.obtenerIndicadores();
  }

  /**
   * obtenerIndicadores
   * 
   * metodo que llama un servicio para obtener los valores.
   */
  async obtenerIndicadores(){

    this.response = await this._peticioService.obtenerindicadores().toPromise();
    if(this.response){
      this.indicadores=this.response
      this.error="OK";
    }else{
      this.error="NOK";
    }
  }

  /**
   * obtenerIndicadoresTest
   * 
   * metodo utilizado para realizar prueba unitaria.Se llama un mock simulando
   * la respuesta de la api.
   * 
   * @param caso corresponde si queremos simular que la api trae los datos correctos, o en el caso
   * de que se produsca algun error. valores: OK,NOK

   */
  obtenerIndicadoresTest(caso:string){

    this.response = this._peticioService.obtenerindicadoresTest(caso);
    if(this.response){
      this.indicadores=this.response
      this.error="OK";
    }else{
      this.error="NOK";
    }
  }

}
