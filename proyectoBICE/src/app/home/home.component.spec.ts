import { async, ComponentFixture, TestBed,fakeAsync, tick} from '@angular/core/testing';
import {PeticionService} from '../services/peticion.service'
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConversorPipe } from '../pipes/conversor.pipe';



import { HomeComponent } from './home.component';

fdescribe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule], 
      declarations: [ HomeComponent,ConversorPipe],
      providers: [PeticionService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create',() => {
   expect(component).toBeTruthy();
  });

  it('caso no OK', (() => {
    fixture.detectChanges();
    component.obtenerIndicadoresTest("NOK");
    fixture.detectChanges();
    expect(component.error).toEqual('NOK');
    fixture.detectChanges();
  
    }));

  it('caso OK', (() => {
    fixture.detectChanges();
    component.obtenerIndicadoresTest("OK");
    fixture.detectChanges();
    expect(component.error).toEqual('OK');
    fixture.detectChanges();
  }));


});
 