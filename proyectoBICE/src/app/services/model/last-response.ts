export interface Indicador{
    dolar:Dolar;
    cobre:Cobre;
    euro:Euro;
    ipc:Ipc;
    ipv:Ipv;
    oro:Oro;
    plata:Plata;
    uf:Uf;
    utm:Utm;
    yen:Yen;
}

export interface Cobre{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;
}

export interface Dolar{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;
}

export interface Euro{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;
}

export interface Ipc{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;
}

export interface Ipv{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;
}

export interface Oro{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;
}

export interface Plata{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;
}

export interface Uf{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;
}

export interface Utm{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;
}

export interface Yen{
    key: string;
    name: string;
    unit: string;
    date: string;
    value: string;
}

