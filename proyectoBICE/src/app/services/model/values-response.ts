export interface ValuesIndicador{
    key:string;
    name:string;
    unit:string;
    values:string;
}
