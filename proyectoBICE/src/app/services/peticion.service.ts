import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Indicador } from './model/last-response';
import {ValuesIndicador} from '../services/model/values-response';
import {ValuesFecha} from '../services/model/date-response';
import { HttpClient }  from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})


/**
 * PeticionService
 * 
 * Componente encargado de la conexion con la api https://www.indecon.onlin.
 * 
 */
@Injectable()
export class PeticionService {

  public url:string;

  constructor(private _http:HttpClient) { 
    this.url = "https://www.indecon.online"; 
  }

  /**
   * obtenerindicadores
   * 
   * Obtiene los ultimos valores registrados de los indicadores.
   * 
   */
  obtenerindicadores(): Observable<any>{
    return Observable.create(async observer =>{
    this.get("/last").subscribe((response:Indicador)=> { 
        console.log("respuesta: " + response.cobre.key);
        if(response){
          observer.next(response);
          observer.complete();
        }else{
          observer.next(false);
          observer.complete();
        }

      },
      err => {
        console.log("Error!!!");
        observer.next(false);
        observer.complete();
      });
    }
    )}

    /**
     * obtenerValores
     * 
     * Se obtiene todos los valores del indicador consultado
     * 
     * @param param indicador a consultar, valores: (/cobre,/dolar,/ipc,/euro,/oro,/plata,/uf,/utm,/ipv,/yen)
     */
    obtenerValores(param:string): Observable<any>{
      return Observable.create(async observer =>{
        this.get("/values" + param).subscribe((response:ValuesIndicador)=> { 
            console.log("respuesta: " + response.key);
            if(response){
              observer.next(response);
              observer.complete();
            }else{
              observer.next(false);
              observer.complete();
            }
    
          },
          err => {
            console.log("Error!!!");
            observer.next(false);
            observer.complete();
          });
        }
        )
    }

    /**
     * obtenerValoresPorfecha
     * 
     * Metodo que obtiene los valores de una fecha e indicador especifico
     * 
     * @param param1 indicador el cual se requiere consultar, valores: (/cobre,/dolar,/ipc,/euro,/oro,/plata,/uf,/utm,/ipv,/yen)
     * @param param2 fecha a consultar(formato fecha: 01-01-2000)
     */
    obtenerValoresPorfecha(param1:string, param2:string): Observable<any>{
      return Observable.create(async observer =>{
        this.get("/date/" + param1 + "/" + param2).subscribe((response:ValuesFecha)=> { 
            console.log("respuesta: " + response.key);
            if(response){
              observer.next(response);
              observer.complete();
            }else{
              observer.next(false);
              
            }
    
          },
          err => {        
            console.log("Error!!!");
            observer.next(false);
            observer.complete();
          });
        }
        )
    }

    /**
     * get
     * 
     * metodo que realiza la conexión con la API
     * 
     * @param param URL de la api
     */
    get(param:string):Observable<{}>{
      return this._http.get(this.url + param);
    }


    /**
     * obtenerindicadoresTest
     * 
     * metodo utilizado para realizar pruebas unitarias, donde se genera un mock de la respuesta de la api
     * @param caso define si queremos que la respuesta sea correcta o incorrecta, valores: OK,NOK
     * 
     */
    obtenerindicadoresTest(caso:string): any{

      if(caso == "OK"){
      const respuestaJson = 
        {
          "cobre": {
              "key": "cobre",
              "name": "Precio del Cobre, dólares por libra",
              "unit": "dolar",
              "date": 1584489600,
              "value": 2.39
          },
          "dolar": {
              "key": "dolar",
              "name": "Dólar observado",
              "unit": "pesos",
              "date": 1584489600,
              "value": 855.09
          },
          "euro": {
              "key": "euro",
              "name": "Euro",
              "unit": "pesos",
              "date": 1584489600,
              "value": 938.42
          },
          "ipc": {
              "key": "ipc",
              "name": "Indice de Precios al Consumidor (Var. c/r al período anterior)",
              "unit": "porcentual",
              "date": 1577836800,
              "value": 1.1
          },
          "oro": {
              "key": "oro",
              "name": "Precio del Oro, dólares por onza",
              "unit": "dolar",
              "date": 1584576000,
              "value": 1473.2
          },
          "plata": {
              "key": "plata",
              "name": "Precio de la Plata, dólares por onza",
              "unit": "dolar",
              "date": 1584576000,
              "value": 11.69
          },
          "uf": {
              "key": "uf",
              "name": "Unidad de fomento",
              "unit": "pesos",
              "date": 1586390400,
              "value": 28630.63
          },
          "utm": {
              "key": "utm",
              "name": "Unidad tributaria mensual",
              "unit": "pesos",
              "date": 1583020800,
              "value": 50021
          },
      };

      return respuestaJson;

    }else{
     return false;
    }
  }

  getTest(param:string):Observable<{}>{
    return this._http.get(param);
  }
}
