import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Importar componnetes
import {HomeComponent} from "./home/home.component";
import {DetalleComponent} from "./detalle/detalle.component";

const appRoutes: Routes =[
	{path: 'home',component: HomeComponent},
	{path: 'detalle',  component: DetalleComponent}
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);