import { async, ComponentFixture, TestBed} from '@angular/core/testing';
import {PeticionService} from '../services/peticion.service'
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConversorPipe } from '../pipes/conversor.pipe';


import { FormsModule} from '@angular/forms'

import { DetalleComponent } from './detalle.component';

describe('DetalleComponent', () => {
  let component: DetalleComponent;
  let fixture: ComponentFixture<DetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,FormsModule], 
      declarations: [ DetalleComponent,ConversorPipe],
      providers: [PeticionService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call api', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();       
      expect(component.error).toEqual('OK');
    });
    
  }));
  

});
