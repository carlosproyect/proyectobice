import { Component, OnInit } from '@angular/core';
import {PeticionService} from '../services/peticion.service';
import{Data} from './data';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})

/**
 * DetalleComponent
 * 
 * Componente que obtiene el detalle historico de cada indicador y lo visualiza al cliente
 * 
 */
export class DetalleComponent implements OnInit {

  public detalle:any;
  private listaValores:Array<string>;
  public unidad:string;
  public valores:Array<Data>
  private _data:Data
  public valorFecha:Data;
  private valuesfecha:any;

  public fecha:string;
  public indice:string;
  public msError:string;

  public maxFecha:string;
  public minFecha:string;

  public maxValor:string;
  public minValor:string;

  public error:string;

  constructor(private _peticioService:PeticionService) { }

  ngOnInit(): void {

    this.error="";

    this.valorFecha = new Data();
    this.valorFecha.nombre = "cobre";
    this.valorFecha.valor = "";
    this.valorFecha.unidad = "dolar";
    this.valorFecha.fecha = "";

    this.msError = "";
    this.obtenerDetalle("/cobre");
  }
  
/**
 * obtenerDetalle
 * 
 * Metodo que llama al servicio que obtiene el detalle de indicador a consultar.
 * 
 * @param indicador indicador que se requiere consultar, valores: (/cobre,/dolar,/ipc,/euro,/oro,/plata,/uf,/utm,/ipv,/yen)
 * 
 */
  async obtenerDetalle(indicador:string){
    this.detalle = await this._peticioService.obtenerValores(indicador).toPromise();

    if(this.detalle){
      this.error="OK";
      var valorJson = this.detalle.values;
      this.valorFecha = new Data();
      this.msError = "";

      this.maxFecha="";
      this.minFecha="";
      this.maxValor="";
      this.minValor="";

      this.unidad=this.detalle.unit;
      this.indice=this.detalle.key;

      this.valorFecha.nombre = this.indice;
      this.valorFecha.unidad = this.unidad;

      var valorSting = JSON.stringify(valorJson);

      valorSting = valorSting.replace(/{/g,'');
      valorSting = valorSting.replace(/}/g,'');
      valorSting = valorSting.replace(/"/g,'');

      this.listaValores = valorSting.split(",");
      this.valores=new Array(this.listaValores.length);
      
      this.ordenarInformacion();

    }else{
      this.error="NOK";
    }
  }

  /**
   * ordenarInformacion
   * 
   * metodo que ordena la informacion de la mas actual a la mas atigua
   * 
   */
  ordenarInformacion(){
    var arr;
    var cont=0;
    for(var i=this.listaValores.length-1; i>=0; i--){
      this._data = new Data();
      arr = this.listaValores[i].split(":");
      this._data.valor=arr[1];
      this._data.fecha=arr[0];
      this.valores[cont]=this._data;
      if(Number(this.maxValor) < Number(this._data.valor)){
        this.maxValor = this._data.valor;
        this.maxFecha = this._data.fecha;
        if(this.minValor==""){
          this.minValor = this._data.valor;
          this.minFecha = this._data.fecha;
        }
      }else if(Number(this.minValor) > Number(this._data.valor)){
        this.minValor = this._data.valor;
        this.minFecha = this._data.fecha;
      }
      cont++;
    }
  }

  /**
   * 
   * Metodo que llama al servicio que obtiene 
   * 
   * @param ind 
   * @param fecha 
   */
  async buscarPorFecha(ind:string,fecha:string){


    fecha = fecha.replace(/\//g , '-');

    this.msError = "";
    this.valuesfecha = await this._peticioService.obtenerValoresPorfecha(ind,fecha).toPromise();
    this.valorFecha = new Data();

    if(this.valuesfecha){
    
      this.valorFecha.nombre = this.valuesfecha.key;
      this.valorFecha.valor = this.valuesfecha.value;
      this.valorFecha.unidad = this.valuesfecha.unit;
      this.valorFecha.fecha = this.valuesfecha.date;

    }

    if((!this.valuesfecha)){
      this.msError = "El formato de la fecha no es el correcto";
    }else if(this.valuesfecha.value == null){
      this.valorFecha.valor = "No se encuentra el valor";
    }

  }

  /**
   * onSubmit
   * 
   * metodo que ejecuta la busqueda de una fecha especifica.
   * 
   */
  onSubmit(){
    this.valorFecha = new Data();
    this.buscarPorFecha(this.indice,this.fecha);

	}
}
