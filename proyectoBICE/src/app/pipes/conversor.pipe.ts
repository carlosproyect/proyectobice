import {Pipe,PipeTransform} from "@angular/core";

@Pipe({name:'conversor'})
export class ConversorPipe implements PipeTransform{
    transform(value){      

        if(value != null && value != ""){
        const milliseconds = 86400000 + (value* 1000);
        const dateObject = new Date (milliseconds);    
        const dateLocal = dateObject.toLocaleString();  
        const dateFinal = dateLocal.split(" ");

        return dateFinal[0];
        }else{
            return "";
        }
    }
}
